package logger

import (
	"fmt"
	"log"
	"syscall"
	"time"
	"unsafe"
)

var (
	globalSet Settings
)

func Disable() {
	close()
}

func IsDebug() bool {
	globalSet.RLock()
	defer globalSet.RUnlock()

	return globalSet.isDebug
}

func Debug(f string, s ...any) {

	if IsDebug() {
		writeLog("debug", fmt.Sprintf(f, s...))
	} else {
		t := time.Now()
		formatT := t.Format("15:04:05")
		log.Printf("%s\t%s\t%s\n", formatT, "debug", s)
	}
}

func Var(f string, s ...any) {
	writeLog("var", fmt.Sprintf(f, s...))
}

func Err(err error) {
	writeLog("err", err.Error())
}

func ShowGuiMsg(title, message string) {
	strPtr := func(str string) uintptr {
		u16, _ := syscall.UTF16PtrFromString(str)
		return uintptr(unsafe.Pointer(u16))
	}

	user32 := syscall.NewLazyDLL("user32.dll")
	msgboxWindow := user32.NewProc("MessageBoxW")
	msgboxWindow.Call(uintptr(0), strPtr(message), strPtr(title), uintptr(0))
}

func CheckAndPanic(err error) {
	if err != nil {
		panic(err)
	}
}
