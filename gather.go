package logger

import (
	"io/fs"
	"log"
	"os"
	"strings"
	"sync"
)

// 读取所有日志
// return map(k: 文件名 v: 文件内容)
// todo: 根据投票情况考虑时候添加日志上传功能
func ReadAllLogContent() (logMap sync.Map) {
	var wg sync.WaitGroup

	entries, osReadDirErr := os.ReadDir(globalSet.logPath)
	if osReadDirErr != nil {
		log.Println(osReadDirErr)
		return
	}
	infos := make([]fs.FileInfo, 0, len(entries))
	for _, entry := range entries {
		info, err := entry.Info()
		if err != nil {
			log.Println(err)
		}
		infos = append(infos, info)
	}
	wg.Add(len(infos))
	for _, fileInfo := range infos {

		go func(fi os.FileInfo) {
			defer wg.Done()

			if strings.Contains(fi.Name(), ".log") {

				bytes, err := os.ReadFile(fi.Name())
				// 搁这还有处理错误的必要吗？
				if err != nil {
					log.Println(err)
				}

				logMap.Store(fi.Name(), bytes)
			}

		}(fileInfo)
	}

	wg.Wait()
	return
}
